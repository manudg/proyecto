﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.ComponentModel;


namespace X
{
    public class BaseViewModel : BaseNotify
    {
        public BaseViewModel()
        {
            Page = null;
        }

        public BaseViewModel(ContentPage page)
        {
            Page = page;
        }


        #region Properties

        public ContentPage Page
        {
            get;
            set;
        }

        private bool _isBusy = false;

        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }
            set
            {
                _isBusy = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("IsNotBusy");
            }
        }

        public bool IsNotBusy
        {
            get { return !IsBusy; }
        }

        private bool _landscape;
        public bool Landscape
        {
            get { return _landscape; }

            set
            {
                _landscape = value;
                NotifyPropertyChanged();
                NotifyPropertyChanged("Portrait");
            }
        }

        public bool Portrait
        {
            get { return !Landscape; }
        }

        #endregion



        public bool CanReload()
        {
            return IsNotBusy;
        }

        protected void SetProperty<T>(
        ref T backingStore, T value,
        string propertyName = "",
        Action onChanged = null,
        Action<T> onChanging = null)
        {


            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return;
            if (onChanging != null)
                onChanging(value);

            OnPropertyChanging(propertyName);
            backingStore = value;

            if (onChanged != null)
                onChanged();

            OnPropertyChanged(propertyName);
        }


        #region INotifyPropertyChanging implementation
        public event Xamarin.Forms.PropertyChangingEventHandler PropertyChanging;
        #endregion

        public void OnPropertyChanging(string propertyName)
        {
            if (PropertyChanging == null)
                return;

            PropertyChanging(this, new Xamarin.Forms.PropertyChangingEventArgs(propertyName));
        }

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged == null)
                return;

            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}